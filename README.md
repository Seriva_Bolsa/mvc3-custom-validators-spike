# Spike: Validaci�n personalizada utilizando Entity Framework Data Annotations

## Autores

* [Claudia Miranda](https://bitbucket.org/cmiranda)
* [Marck Lino](https://bitbucket.org/mlinoverdi)
* [Juan Jos� Fuchs](https://bitbucket.org/JuanjoFuchs)

## Objetivo

* Encontrar una manera de incorporar validaciones a las entidades que viajan a traves de todas las capas de la aplicaci�n, pero sin contaminarlas con librer�as ni c�digo de GUI.

## Referencias

* [ASP.NET MVC3 Validation Basic](www.codeproject.com/Articles/249452/ASP-NET-MVC3-Validation-Basic)
* [Custom Unobtrusive jQuery Validation with Data Annotations in MVC 3](http://thewayofcode.wordpress.com/2012/01/18/custom-unobtrusive-jquery-validation-with-data-annotations-in-mvc-3/)

## C�digo

### 1. Creamos un proyecto `Entidades` de tipo Class Library y dentro de �l agregamos una clase `Orden.cs`
Esta entidad contiene las validaciones que le corresponden solo a ella y que viajaran con ella a todos los proyectos donde sea usada.

Archivo: [Orden.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/Entidades/Orden.cs)

	:::csharp
	namespace Entidades
	{
		public class Orden
		{
			public string Valor { get; set; }

			[Required(ErrorMessage = "Debe especificar una cantidad")]
			[MayorQue(0, "La cantidad debe ser mayor a 0")]
			public int Cantidad { get; set; }

			public Cliente Cliente { get; set; }
		}
	}


### 2. Creamos un atributo para validar que la Cantidad sea mayor a 0
Este atributo implementa la clase `ValidationAttribute`

Arhivo: [MayorQueAttribute.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/Entidades/Attributes/MayorQueAttribute.cs)

	:::csharp
	namespace Entidades.Attributes
	{
		public class MayorQueAttribute : ValidationAttribute
		{
			protected readonly int Minimo;

			public MayorQueAttribute(int minimo, string mensajeError)
				: base(mensajeError)
			{
				Minimo = minimo;
			}

			protected override ValidationResult IsValid(object value, ValidationContext validationContext)
			{
				int numero;

				if (!int.TryParse(value.ToString(), out numero))
					return new ValidationResult("El valor no es un numero");

				if (numero <= Minimo)
					return new ValidationResult(ErrorMessageString);

				return ValidationResult.Success;
			}

		}
	}

### 3. Creamos un proyecto de tipo ASP.NET MVC3 y utilizamos el modelo en la vista `Index.cshtml`
A partir de ahora nuestro modelo validara que el campo cantidad sea mayor a 0 cada vez que se haga postback

Archivo: [Index.cshtml](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/MVC3CustomValidatorsKata/Views/Home/Index.cshtml)

	:::html
	@using (Html.BeginForm())
	{
		<div>
			@Html.TextBoxFor(model => model.Cantidad)
			@Html.ValidationMessageFor(model => model.Cantidad)
		</div>
		<div>
			<input type="submit" value="Crear" />
		</div>
	}

Archivo: [HomeController.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/MVC3CustomValidatorsKata/Controllers/HomeController.cs)

	:::csharp
	public class HomeController : Controller
	{
		[HttpPost]
		public ActionResult Index(OrdenViewModel orden)
		{
			if (!ModelState.IsValid)
			{
				return View(orden);
			}
			return RedirectToAction("Success");
		}
	}

### 4. Para que la validacion de Cantidad pueda hacerse de manera as�ncrona, en el proyecto MVC, agregamos el archivo `Attributes/MayorQueClientAttribute.cs`
En esta clase implementaremos la interfaz `IClientValidatable` que permitir� que nuestra validacion sea llamada desde JQuery.
La clase que contiene la implementaci�n de la interfaz `IClientValidatable` se hizo en el proyecto web porque no es correcto contaminar el proyecto de entidades con librer�as y c�digo de GUI.

Archivo: [MayorQueClientAttribute.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/MVC3CustomValidatorsKata/Attributes/MayorQueClientAttribute.cs)

	:::csharp
	public class MayorQueClientAttribute : MayorQueAttribute, IClientValidatable
	{
		public MayorQueClientAttribute(int minimo, string mensajeError) 
			: base(minimo, mensajeError)
		{
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var mayorQue = new ModelClientValidationRule { ErrorMessage = ErrorMessageString, ValidationType = "mayorque" };
			mayorQue.ValidationParameters.Add("minimo", Minimo);

			yield return mayorQue;
		}
	}

### 5. Para que nuestra entidad `Orden.cs` soporte la funcionalidad de validaci�n de Cantidad por JQuery, necesitamos extender sus propiedades
Para esto hacemos uso del atributo `MetadataType` que nos permite extender los atributos de validaci�n de propiedades de una clase. Usualmente se utilizan clases parciales para usar esta funcionalidad, pero nosotros estamos usando herencia porque nuestra entidad `Orden.cs` est� en otra librer�a.

Archivo: [OrdenModel.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/41047aca98b6/MVC3CustomValidatorsKata/Models/OrdenModel.cs)

	:::csharp
	[MetadataType(typeof(OrdenMetadata))]
	public class OrdenModel : Orden
	{
	}

	public class OrdenMetadata
	{
		[MayorQueClient(0, "La cantidad debe ser mayor a 0")]
		public object Cantidad { get; set; }
	}

### 6. Agregamos el c�digo JavaScript necesario para que las validaciones sucedan en el cliente

Archivo: [CustomValidations.js](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/6147ac496742/MVC3CustomValidatorsKata/Scripts/CustomValidations.js)

	:::javascript
	$.validator.addMethod("mayorque", function (value, element, params) {
		return parseInt(value) > parseInt(params);
	});

	$.validator.unobtrusive.adapters.add("mayorque", ["minimo"], function (options) {
		options.rules["mayorque"] = options.params.minimo;
		options.messages["mayorque"] = options.message;
	});
	
### 7. Referenciamos el modelo extendido `OrdenModel.cs` en la vista `Index.cshtml` y agregamos las referencias al codigo JavaScript necesario
A partir de ahora la validaci�n de cantidad puede ser ejecutada desde el cliente y sin necesidad de hacer postback.

Archivo: [Index.cshtml](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/MVC3CustomValidatorsKata/Views/Home/Index.cshtml)

	:::html
	@model MVC3CustomValidatorsKata.Models.OrdenModel
	<script src="@Url.Content("~/Scripts/jquery.validate.min.js")" type="text/javascript"></script>
	<script src="@Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js")" type="text/javascript"></script>
	<script src="@Url.Content("~/Scripts/CustomValidations.js")" type="text/javascript"></script>
	@using (Html.BeginForm())
	{
		<div>
			@Html.Hidden("idcCliente", Model.Cliente.IDC)
			@Html.TextBoxFor(model => model.Cantidad)
			@Html.ValidationMessageFor(model => model.Cantidad)
		</div>
		<div>
			<input type="submit" value="Crear" />
		</div>
	}
	
### 8. Necesitamos agregar una validaci�n m�s, y esta es que el cliente tenga en un salgo mayor a la cantidad especificada en la orden
Para esto utilizaremos el atributo `Remote` de los Data Annotations ya que este nos permite consumir el m�todo de un controlador para esta validaci�n
En este caso consumiremos la acci�n `ValidarSaldoCliente` de nuestro `HomeController`.
Se ha implementado esta validaci�n haciendo uso del controlador porque la verificaci�n del saldo del cliente **no le pertenece** a la entidad `Orden.cs`, mas bien es una necesidad de la acci�n de "Crear Orden".
Esta validaci�n tambi�n es as�ncrona porque el atributo `Remote` lo permite.

Archivo: [OrdenModel.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/41047aca98b6/MVC3CustomValidatorsKata/Models/OrdenModel.cs)

	:::csharp
    public class OrdenMetadata
    {
        [Remote("ValidarSaldoCliente", "Home", AdditionalFields = "idcCliente")]
        public object Cantidad { get; set; }
    }

Archivo: [HomeController.cs](https://bitbucket.org/Seriva_Bolsa/mvc3-custom-validators-spike/src/c2a54920e51e/MVC3CustomValidatorsKata/Controllers/HomeController.cs)

	:::csharp
	public ActionResult ValidarSaldoCliente(int cantidad, string idcCliente)
	{
		// Consultar el repositorio de clientes
		Cliente cliente = null;
		if (Clientes.Claudia.IDC == idcCliente)
			cliente = Clientes.Claudia;

		if (cliente != null && cliente.Saldo >= cantidad)
			return Json(true, JsonRequestBehavior.AllowGet);

		return Json(string.Format("El saldo del cliente con IDC {0} no es suficiente para esta operacion", idcCliente), JsonRequestBehavior.AllowGet);
	}
