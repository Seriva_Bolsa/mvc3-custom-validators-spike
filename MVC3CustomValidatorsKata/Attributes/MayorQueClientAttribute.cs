﻿using System.Collections.Generic;
using System.Web.Mvc;
using Entidades.Attributes;

namespace MVC3CustomValidatorsKata.Attributes
{
    public class MayorQueClientAttribute : MayorQueAttribute, IClientValidatable
    {
        public MayorQueClientAttribute(int minimo, string mensajeError) 
            : base(minimo, mensajeError)
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var mayorQue = new ModelClientValidationRule { ErrorMessage = ErrorMessageString, ValidationType = "mayorque" };
            mayorQue.ValidationParameters.Add("minimo", Minimo);

            yield return mayorQue;
        }
    }
}