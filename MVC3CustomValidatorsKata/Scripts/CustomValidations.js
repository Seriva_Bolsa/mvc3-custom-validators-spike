﻿$.validator.addMethod("mayorque", function (value, element, params) {
    return parseInt(value) > parseInt(params);
});

$.validator.unobtrusive.adapters.add("mayorque", ["minimo"], function (options) {
    options.rules["mayorque"] = options.params.minimo;
    options.messages["mayorque"] = options.message;
});