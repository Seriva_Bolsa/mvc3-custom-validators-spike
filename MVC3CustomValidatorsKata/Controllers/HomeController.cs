﻿using System.Web.Mvc;
using Entidades;
using MVC3CustomValidatorsKata.Models;
using MVC3CustomValidatorsKata.Repositorio;

namespace MVC3CustomValidatorsKata.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var orden = new OrdenModel
                              {
                                  Valor = "GOOG",
                                  Cliente = Clientes.Claudia
                              };

            return View(orden);
        }

        public ActionResult About()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(OrdenModel orden)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("About");
            }
            return View(orden);
        }

        public ActionResult ValidarSaldoCliente(int cantidad, string idcCliente)
        {
            // Consultar el repositorio de clientes
            Cliente cliente = null;
            if (Clientes.Claudia.IDC == idcCliente)
                cliente = Clientes.Claudia;

            if (cliente != null && cliente.Saldo >= cantidad)
                return Json(true, JsonRequestBehavior.AllowGet);

            return Json(string.Format("El saldo del cliente con IDC {0} no es suficiente para esta operacion", idcCliente), JsonRequestBehavior.AllowGet);
        }
    }
}
