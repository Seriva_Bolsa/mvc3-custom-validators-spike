using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Entidades;
using MVC3CustomValidatorsKata.Attributes;

namespace MVC3CustomValidatorsKata.Models
{
    [MetadataType(typeof(OrdenMetadata))]
    public class OrdenModel : Orden
    {
    }

    public class OrdenMetadata
    {
        [Remote("ValidarSaldoCliente", "Home", AdditionalFields = "idcCliente")]
        [MayorQueClient(0, "La cantidad debe ser mayor a 0")]
        public object Cantidad { get; set; }
    }
}