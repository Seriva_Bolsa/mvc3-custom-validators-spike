﻿using Entidades;

namespace MVC3CustomValidatorsKata.Repositorio
{
    public class Clientes
    {
        public static Cliente Claudia
        {
            get
            {
                return new Cliente
                           {
                               IDC = "123",
                               Saldo = 50
                           };
            }
        }
    }
}