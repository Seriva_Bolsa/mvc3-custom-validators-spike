﻿using System.ComponentModel.DataAnnotations;

namespace Entidades.Attributes
{
    public class MayorQueAttribute : ValidationAttribute
    {
        protected readonly int Minimo;

        public MayorQueAttribute(int minimo, string mensajeError)
            : base(mensajeError)
        {
            Minimo = minimo;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int numero;

            if (!int.TryParse(value.ToString(), out numero))
                return new ValidationResult("El valor no es un numero");

            if (numero <= Minimo)
                return new ValidationResult(ErrorMessageString);

            return ValidationResult.Success;
        }

    }
}
