﻿using System.ComponentModel.DataAnnotations;
using Entidades.Attributes;

namespace Entidades
{
    public class Orden
    {
        public string Valor { get; set; }

        [Required(ErrorMessage = "Debe especificar una cantidad")]
        [MayorQue(0, "La cantidad debe ser mayor a 0")]
        public int Cantidad { get; set; }

        public Cliente Cliente { get; set; }
    }
}
